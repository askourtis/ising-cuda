# Constants
SHELL = /bin/bash
NVCC  = nvcc

SRC_DIR = ./src/
BLD_DIR = ./build/
INC_DIR = ./inc/
LIB_DIR = ./lib/

COMMON_CUDA_EXT = -cuda

# Compiler flags
FLAGS = -D NDEBUG -O3 -I$(INC_DIR)

# Files
ISING_CUDA_SOURCES := $(shell find $(SRC_DIR) -type f -name "ising-cuda-*.cu")
ISING_CUDA_OBJECTS := $(ISING_CUDA_SOURCES:$(SRC_DIR)%.cu=$(BLD_DIR)%.o)

ISING_C_SOURCES = $(SRC_DIR)ising-seq.c
ISING_C_OBJECTS := $(ISING_C_SOURCES:$(SRC_DIR)%.c=$(BLD_DIR)%.o)

COMMON_SOURCES = $(SRC_DIR)alg.c
COMMON_OBJECTS := $(COMMON_SOURCES:$(SRC_DIR)%.c=$(BLD_DIR)%.o)


# Rules
all: | clean lib

# Construct libs
.PHONY: lib
lib: compile $(LIB_DIR)
	$(NVCC) -lib $(BLD_DIR)ising-cuda-thread.o  $(BLD_DIR)alg$(COMMON_CUDA_EXT).o -o $(LIB_DIR)ising-cuda-thread.a
	$(NVCC) -lib $(BLD_DIR)ising-cuda-blocked.o $(BLD_DIR)alg$(COMMON_CUDA_EXT).o -o $(LIB_DIR)ising-cuda-blocked.a
	$(NVCC) -lib $(BLD_DIR)ising-cuda-shared.o  $(BLD_DIR)alg$(COMMON_CUDA_EXT).o -o $(LIB_DIR)ising-cuda-shared.a
	$(NVCC) -lib $(BLD_DIR)ising-seq.o          $(BLD_DIR)alg.o                   -o $(LIB_DIR)ising-seq.a


# Compile needed object files
.PHONY: compile
compile: | $(BLD_DIR) $(ISING_CUDA_OBJECTS) $(ISING_C_OBJECTS) $(COMMON_OBJECTS)

# Make Directories
$(BLD_DIR) $(LIB_DIR):
	mkdir $@

# Compile cuda files
$(BLD_DIR)%.o: $(SRC_DIR)%.cu
	$(NVCC) $(FLAGS) -dc $^ -o $@

# Compile c and common files
$(BLD_DIR)%.o: $(SRC_DIR)%.c
	$(NVCC) $(FLAGS) -c $^ -o $@
	@if [[ $(COMMON_SOURCES) == *"$^"* ]]; then\
		CMD="$(NVCC) $(FLAGS) -dc --x=cu $^ -o $(patsubst %.o,%-cuda.o,$@)";\
		echo $$CMD;\
		$$CMD;\
	fi

# Clean up
.PHONY: clean
clean:
	rm -rf $(BLD_DIR)
	rm -rf $(LIB_DIR)