/**
 * @file alg.h
 * @author Antonis Skourtis
 * @brief  Declarations for useful algorithms
 * @date 2019-12-25
 */
#ifndef ALG_H_GUARD
#define ALG_H_GUARD

#include "def.h"

/**
 * @brief Calculates the weighted sum of the specific point in the grid
 *
 * @param G The grid
 * @param i The row index
 * @param j The column index
 * @param n The leading dimension of G
 * @param W The weights
 *
 * @return the weighted sum
 *
 * @note Works for both device and host
 */
GLOBAL double calculate_weighted_sum(const int *G, int i, int j, int n, const double *W);

/**
 * @brief Updates to a buffer a moment of the grid. Returns wether or not there was an update
 *
 * @param G The grid
 * @param i The row index
 * @param j The column index
 * @param n The leading dimension of G
 * @param W The weights
 *
 * @return The sign of the calculated moment
 *
 * @note Works for both device and host
 */
GLOBAL int calculate_moment(const int *G, int i, int j, int n, const double *W);

#endif
//-----------EOF-----------//