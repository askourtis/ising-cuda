/**
 * @file    defs.h
 * @author  Antonis Skourtis
 * @brief   Useful inlines, macros and constants
 * @date    2019-12-22
 */

#ifndef DEFS_H_GUARD
#define DEFS_H_GUARD
#include <stddef.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
//-----------GENERAL-----------//

/**
 * @brief INLINE keyword definition
 */
#ifdef __CUDACC__
#define INLINE __forceinline__
#else
#define INLINE static inline
#endif

/**
 * @brief GLOBAL keyword definition
 */
#ifdef __CUDACC__
#define GLOBAL __device__ __host__
#else
#define GLOBAL
#endif


/**
 * @brief Row-Major index conversion
 *
 * @param i The row index
 * @param j The column index
 * @param d The leading dimension
 *
 * @return  The respective 1D index
 */
GLOBAL INLINE size_t idx(size_t const i, size_t const j, size_t d) {
    return i*d+j;
}

/**
 * @brief Generic inline swap. Swaps two variables
 */
#define SWAP(x,y)   do{                          \
                        typeof(x) x##y=x;        \
                        x=y;                     \
                        y=x##y;                  \
                    }while(0)


//-----------MATH-----------//

/**
 * @brief Generic inline mathematical square
 */
#define SQUARE(x) ((x)*(x))

/**
 * @brief Generic inline mathematical max
 */
#define MAX(x,y)  ((x)>(y)?(x):(y))

/**
 * @brief Generic inline mathematical min
 */
#define MIN(x,y)  ((x)<(y)?(x):(y))

/**
 * @brief The mathematical modulo operation
 *
 * @param x First operant
 * @param m Second operant
 *
 * @return x mod m
 */
GLOBAL INLINE int mod(int const x, int const m) {
    return ((x%m)+m)%m;
}

/**
 * @brief Evaluates the signed of a number
 *
 * @param x The given number
 *
 * @return 1 for positive numbers, -1 for negatives, 0 for zeros
 */
GLOBAL INLINE int sign(double const x) {
    return (x>0) - (x<0);
}

/**
 * @brief An equality for floating point numbers
 *
 * @param x   First operand
 * @param y   Second operand
 * @param err Acceptable error
 *
 * @return Whether the two operands are within the acceptable error
 */
GLOBAL INLINE bool almost_equals(double const x, double const y, double const err) {
    const double z = x-y;
    return (sign(z)*z)<err;
}

/**
 * @brief Evaluates if the given integer is even or not
 *
 * @param x An integer
 *
 * @return true if the given integer was even, false otherwise
 */
GLOBAL INLINE bool even(int const x) {
    return x%2 == 0;
}

/**
 * @brief Evaluates if the given integer is odd or not
 *
 * @param x An integer
 *
 * @return true if the given integer was odd, false otherwise
 */
GLOBAL INLINE bool odd(int const x) {
    return !even(x);
}


//-----------ERROR HANDLING-----------//

/**
 * @brief Enumeration for error codes
 */
enum error {
    MEMERR=1,
    CUDERR
};

/**
 * @brief   Terminates execution
 *
 * @param error     Error code
 * @param format    A formated message
 * @param ...       The arguments to the message
 *
 * @return does not return
 */
static void fatal(enum error const error, const char* const format, ...) {
    va_list args;
    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);
    exit(error);
}

#define NULL_CHECK(expr) null_check(expr, __FILE__, __LINE__);
INLINE void null_check(const void* const ptr, const char *const file, int const line) {
    if(ptr == NULL)
        fatal(MEMERR, "%s:%d Could not allocate memory", file, line);
}

#define CUDA_CHECK(expr) cuda_check(expr, __FILE__, __LINE__);
INLINE void cuda_check(int const err, const char *const file, int const line) {
#ifdef __CUDACC__
    if(err != cudaSuccess)
        fatal(CUDERR, "%s:%d Cuda failed(%d): %s", file, line, err, cudaGetErrorName((cudaError_t)err));
#endif
}

//-----------DEBUG MODE-----------//
#ifndef NDEBUG
//Acts like printf
#define DEBUG_LOG(...) fprintf(stdout, __VA_ARGS__)
#else
//Does nothing
#define DEBUG_LOG(...) ((void)0)
#endif

//-----------CONSTANTS-----------//
//Weights leading dimension
#define WLD           5
//Half of the WLD
#define H_WLD         (WLD/2)
//Threshold for the almost_equals function
#define THRESHOLD   1e-8
//Maximum allowed threads per block
#define MAX_THREADS 1024
//Arbitrary number of threads per block
#define THREADS 128

#endif
//-----------EOF-----------//