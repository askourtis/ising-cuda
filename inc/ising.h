/**
 * @file    ising.h
 * @author  Antonis Skourtis
 * @brief   Declarations for the sequential ising algorithm
 * @date    2019-12-22
 */

#ifndef ISING_H_GUARD
#define ISING_H_GUARD

#ifdef __CUDACC__
#define EXTERN extern "C"
#else
#define EXTERN extern
#endif

//------Declarations------//

//! Ising model evolution
/*!

  \param G      Spins on the square lattice             [n-by-n]
  \param w      Weight matrix                           [5-by-5]
  \param k      Number of iterations                    [scalar]
  \param n      Number of lattice points per dim        [scalar]

  NOTE: Both matrices G and w are stored in row-major format.
*/
EXTERN void ising(int* G, double* w, int k, int n);

#undef EXTERN
#endif
//-----------EOF-----------//