# Ising-Cuda

Implementing in CUDA the evolution of an Ising model in two dimensions for a given number of steps 𝑘.

## How to build

First download the entire project to your local disk, by typing into a terminal the following:
```
git clone https://gitlab.com/askourtis/ising-cuda.git
```

Navigate to the project directory via:
```
cd $project
```
<sup><sub>Variable $project should be replaced with the actuall directory</sub></sup>

Then to build the libraries type:
```
make
```

This will clean the working directory and build the libraries from scratch.

If you dont want to clean the entire directory and rebuild everything. Try using:
```
make lib
```

This will not clean the working directory, and only build the updated libraries

**NOTE:** The cuda libraries might take a while to compile.
