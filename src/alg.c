/**
 * @file alg.c
 * @author Antonis Skourtis
 * @brief  Definitions for useful algorithms
 * @date 2019-12-25
 */
#include "alg.h"

double calculate_weighted_sum(const int *const G, int const i, int const j, int const n, const double *const W) {
    double wsum = 0.0;
    for(int di=-H_WLD; di<=+H_WLD; ++di) {
        for(int dj=-H_WLD; dj<=+H_WLD; ++dj) {
            if(dj==0 && di==0)
                continue;
            wsum += W[idx(di+H_WLD, dj+H_WLD, WLD)] * G[idx(mod(i+di,n), mod(j+dj,n), n)];
        }
    }
    return wsum;
}

int calculate_moment(const int *const G, int const i, int const j, int const n, const double *const W) {
    const double wsum = calculate_weighted_sum(G, i, j, n, W);

    //If wsum is not of significance
    if(almost_equals(wsum, 0.0, THRESHOLD))
        return G[idx(i,j,n)];

    return sign(wsum);
}
//-----------EOF-----------//