/**
 * @file    ising-cuda-blocked.c
 * @author  Antonis Skourtis
 * @brief   Definitions for the blocked ising algorithm
 * @date    2019-12-25
*/
#include "ising.h"
#include "alg.h"
#include <assert.h>
#include <thrust/equal.h>
#include <thrust/execution_policy.h>

#define BLOCK_DIM 16
#define GRID_DIM  33

//-------Constants-------//

/**
 * @brief The size of the W matrix
 */
 #define W_SIZE SQUARE((size_t)WLD)

 /**
  * @brief W matrix in constant memory
  */
 __device__ __constant__
 static double const_W[W_SIZE];

//------Helper Declarations------//

/**
 * @brief Updates all the points of the given grid to the given buffer
 *
 * @param G - The grid
 * @param B - The Buffer
 * @param W - The weights
 * @param n - The leading dimension of the grid
 *
 * @note Use square blocks and grid
 *
 */
__global__
static void update_grid(const int *G, int *B, int n);

/**
 * @brief Loads a tile of memory
 *
 * @param D      The destination buffer
 * @param S      The source buffer
 * @param i_off  The row offset from the source buffer
 * @param j_off  The column offset from the source buffer
 * @param dld    The leading dimension of the destination buffer
 * @param sld    The leading dimension of the source buffer
 * @param stride [Optional] The stride for the loops
 *
 * @note Use square blocks and grid
 *
 */
__device__
static void load_mem(int *D, const int *S, int i_off, int j_off, int dld, int sld, int stride=1);

//-------Definitions-------//
void ising(int* const G, double* const W, int const k, int const n) {
    assert(G != NULL);
    assert(W != NULL);
    assert(k > 0);
    assert(n > 0);

    //Size of the arrays
    size_t const size = SQUARE((size_t)n);

    //Allocate Device Buffer
    int* d_B;
    CUDA_CHECK( cudaMalloc(&d_B, size*sizeof(int)) );

    //Allocate Device Grid
    int* d_G;
    CUDA_CHECK( cudaMalloc(&d_G, size*sizeof(int)) );

    //Copy to Device the Grid
    CUDA_CHECK( cudaMemcpy(d_G, G, size*sizeof(int), cudaMemcpyHostToDevice) );
    //Copy to Device Constant the Weights
    CUDA_CHECK( cudaMemcpyToSymbol(const_W, W, W_SIZE*sizeof(double)) );

    bool changed;
    int epoch;
    {
        int* prev = d_G;
        int* next = d_B;
        //For k iterations until no changes
        for(epoch=0, changed=true; changed && epoch<k; ++epoch) {
            //Update the grid
            update_grid<<<dim3(GRID_DIM,GRID_DIM), dim3(BLOCK_DIM,BLOCK_DIM), SQUARE(WLD+BLOCK_DIM)*sizeof(int)>>>(prev, next, n);
            //Check for kernel errors
            CUDA_CHECK( cudaPeekAtLastError() );
            //Wait for the kernel to finish
            CUDA_CHECK( cudaDeviceSynchronize() );
            //Check if changes happend
            changed = !thrust::equal(thrust::device, d_G, d_G+(size), d_B);
            //Swap device buffers
            SWAP(prev, next);
            //Repeat...
        }
    }

    //Copy correct buffer to the Host grid
    CUDA_CHECK( cudaMemcpy(G, (changed && odd(epoch))?d_B:d_G, size*sizeof(int), cudaMemcpyDeviceToHost) );

    //Clean up
    CUDA_CHECK( cudaFree(d_B)     );
    CUDA_CHECK( cudaFree(d_G)     );
}

//-------Helper Definitions-------//

__global__
void update_grid(const int *const G, int *const B, int const n) {
    //Assert 2D square block
    assert(blockDim.z==1);
    assert(blockDim.x==blockDim.y);
    //Assert 2D square grid
    assert(gridDim.z==1);
    assert(gridDim.x==gridDim.y);

    extern __shared__ int shared_tile[];

    int const block_dim = blockDim.x;
    int const grid_dim  = gridDim.x;

    int const stride = block_dim*grid_dim;

    int const y_off = blockIdx.y*block_dim;
    int const x_off = blockIdx.x*block_dim;

    int const sld = block_dim+WLD;

    for(int y=y_off; y<n; y+=stride) {
        for(int x=x_off; x<n; x+=stride) {

            load_mem(shared_tile, G, y-H_WLD, x-H_WLD, sld, n, block_dim);
            __syncthreads();

            int const i = y + threadIdx.y;
            int const j = x + threadIdx.x;

            if(i<n && j<n)
                B[idx(i,j,n)] = calculate_moment(shared_tile, threadIdx.y+H_WLD, threadIdx.x+H_WLD, sld, const_W);
            __syncthreads();
        }
    }
}

__device__
void load_mem(int *const D, const int *const S, int const i_off, int const j_off, int const dld, int const sld, int const stride) {
    for(int i=threadIdx.y; i<dld; i+=stride)
        for(int j=threadIdx.x; j<dld; j+=stride)
            D[idx(i,j,dld)] = S[idx(mod(i_off+i,sld),mod(j_off+j,sld),sld)];
}
//-----------EOF-----------//