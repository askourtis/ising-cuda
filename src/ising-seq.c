/**
 * @file    ising-seq.c
 * @author  Antonis Skourtis
 * @brief   Definitions for the sequential ising algorithm
 * @date    2019-12-22
 */

#include "ising.h"
#include "alg.h"
#include <assert.h>
#include <memory.h>

//-------Definitions-------//
void ising(int* const G, double* const W, int const k, int const n) {
    assert(G != NULL);
    assert(W != NULL);
    assert(k > 0);
    assert(n > 0);

    //Size of the arrays
    size_t size = SQUARE((size_t)n);

    int* const buffer = (int*)malloc(size*sizeof(int));
    NULL_CHECK( buffer );


    int epoch;
    bool changed;
    {
        int* prev = G;
        int* next = buffer;
        for(epoch=0, changed=true; changed && epoch<k; ++epoch) {

            //Globally assume no changes were made
            changed = false;

            //For each point in the grid
            for(int i=0; i<n; ++i)
                for(int j=0; j<n; ++j) {
                    next[idx(i,j,n)] = calculate_moment(prev, i, j, n, W);
                    changed |= next[idx(i,j,n)] != prev[idx(i,j,n)];
                }

            //Swap the buffers
            SWAP(next, prev);
        }
    }

    //If epoch is odd and there is a difference between the buffers, update the G buffer
    if(changed && odd(epoch))
        memcpy(G, buffer, size*sizeof(int));

    //Clean up
    free(buffer);
}
//-----------EOF-----------//