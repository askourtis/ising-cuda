/**
 * @file    ising-cuda-blocked.c
 * @author  Antonis Skourtis
 * @brief   Definitions for the blocked ising algorithm
 * @date    2019-12-25
*/
#include "ising.h"
#include "alg.h"
#include <assert.h>
#include <thrust/equal.h>
#include <thrust/execution_policy.h>

#define BLOCK_DIM 16
#define GRID_DIM  33

//-------Constants-------//

/**
 * @brief The size of the W matrix
 */
 #define W_SIZE SQUARE((size_t)WLD)

 /**
  * @brief W matrix in constant memory
  */
 __device__ __constant__
 static double const_W[W_SIZE];

//------Helper Declarations------//

/**
 * @brief Updates all the points of the given grid to the given buffer
 *
 * @param G - The grid
 * @param B - The Buffer
 * @param W - The weights
 * @param n - The leading dimension of the grid
 *
 * @note Use square blocks and grid
 *
 */
__global__
static void update_grid(const int *G, int *B, int n);


//-------Definitions-------//
void ising(int* const G, double* const W, int const k, int const n) {
    assert(G != NULL);
    assert(W != NULL);
    assert(k > 0);
    assert(n > 0);

    //Size of the arrays
    size_t const size = SQUARE((size_t)n);

    //Allocate Device Buffer
    int* d_B;
    CUDA_CHECK( cudaMalloc(&d_B, size*sizeof(int)) );

    //Allocate Device Grid
    int* d_G;
    CUDA_CHECK( cudaMalloc(&d_G, size*sizeof(int)) );

    //Copy to Device the Grid
    CUDA_CHECK( cudaMemcpy(d_G, G, size*sizeof(int), cudaMemcpyHostToDevice) );
    //Copy to Device Constant the Weights
    CUDA_CHECK( cudaMemcpyToSymbol(const_W, W, W_SIZE*sizeof(double)) );

    bool changed;
    int epoch;
    {
        int* prev = d_G;
        int* next = d_B;
        //For k iterations until no changes
        for(epoch=0, changed=true; changed && epoch<k; ++epoch) {
            //Update the grid
            update_grid<<<dim3(GRID_DIM,GRID_DIM), dim3(BLOCK_DIM,BLOCK_DIM)>>>(prev, next, n);
            //Check for kernel errors
            CUDA_CHECK( cudaPeekAtLastError() );
            //Wait for the kernel to finish
            CUDA_CHECK( cudaDeviceSynchronize() );
            //Check if changes happend
            changed = !thrust::equal(thrust::device, d_G, d_G+(size), d_B);
            //Swap device buffers
            SWAP(prev, next);
            //Repeat...
        }
    }

    //Copy correct buffer to the Host grid
    CUDA_CHECK( cudaMemcpy(G, (changed && odd(epoch))?d_B:d_G, size*sizeof(int), cudaMemcpyDeviceToHost) );

    //Clean up
    CUDA_CHECK( cudaFree(d_B)     );
    CUDA_CHECK( cudaFree(d_G)     );
}

//-------Helper Definitions-------//

__global__
void update_grid(const int *const G, int *const B, int const n) {
    //Assert 2D square block
    assert(blockDim.z==1);
    assert(blockDim.x==blockDim.y);
    //Assert 2D square grid
    assert(gridDim.z==1);
    assert(gridDim.x==gridDim.y);

    int const block_dim = blockDim.x;
    int const grid_dim  = gridDim.x;

    int const stride = block_dim*grid_dim;


    for(int i=idx(blockIdx.y,threadIdx.y,block_dim); i<n; i+=stride)
        for(int j=idx(blockIdx.x,threadIdx.x,block_dim); j<n; j+=stride)
            B[idx(i,j,n)] = calculate_moment(G, i, j, n, const_W);

}
//-----------EOF-----------//